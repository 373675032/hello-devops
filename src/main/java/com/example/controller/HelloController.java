package com.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author XUEW
 * @since 2023/12/5 15:41
 */
@RestController
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        return "Hello CI/CD";
    }
}
